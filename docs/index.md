

**<span style="color:Black;font-size:1.2em">Wadau Portal </span>**
![](img/wadau.PNG)
<p align ="justify">The “Implementing Partners Management Portal” is a Web-based application which helps PO-RALG and the Government to:- </p>

+ Identify & Locate 
+  Govern
+  Monitor
+  Evaluate
+  Appraise & Assess 
+  Applaud
+  Reference for best practices ALL/ANY implementing partner(s) across the country.

<p align ="justify">The system is available through http://41.59.1.86 </p>
![](img/map_wadau.PNG)


