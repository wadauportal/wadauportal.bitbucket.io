** <span style="color:Black;font-size:1.5em">Patners Registration</span>**
<p><span style = "color:red">**What are the necessary informations needed during registration of new partner in the Portal ?**</span></p>
<p>To register new partner you must have four core informations.</p>

1. ** <span>Implementer Informations</span>** e.g Registration Number, Name, Address, Email, Contact and Location Information.
2.   ** <span>Project Informations</span>** e.g Project Name, Short form, Sector, Funder, Total fund in USD, Project start date, Project end date, Priority area and National Guideline.
3.   ** <span>Project Documents</span>** e.g Introduction Letter and Memorandum of Understanding.
4.   ** <span>Project Scope</span>** e.g Project Location - where the project is implemented.


**<span style="color:Black;font-size:1.2em">Implementer Information</span>**

<p align="justify">In order to get started firstly open your browser, prefferable Google chrome, then enter <a href="http://41.59.1.86"> http://41.59.1.86</a>  address on the browser then the system will open as shown below.</p>

![](img/portal.PNG)


Then click ** <span>Partner Registration </span>** you will see the following screen on your browser.

![](img/patner.PNG)

**<span style="color:Black;font-size:1.2em">1. Implementer Information</span>**

   i.** <span>Registration Number. </span>** - This is the Implementer Registration Number which is mandatory and  recognized by the government. This will validate informations provided by user whether are valid or not. Implementer must make sure Registration Number is correct and recognized.

   ii.** <span>Name </span>** - Implementer Name is mandatory field  which accept Name of the Registered Implementer as shown in the diagram below. 

   iii.** <span>Address</span>**- During registration user must provide the valid address that are valid and currently operated in the country, for example P.O.BOX 3567 Dar es salaam. 

   iv.** <span>Email </span>** - Email field must be valid and reachable, upon successfully confirmation of the informations provided by implementer system will validate some informations by using the email provided on this field. If the email is wrong you will not able to access the wadau portal system. <span style = "color:red">**Please make sure email is correct and it working **</span>

   v.** <span>Contact </span>** - This field receive implementer phone number. The number supplied here must be reachable and correctly typed.

   vi.** <span>Location </span>** - Headquarter location, information provided here must show where exactly the implementer can be found. for example Mikocheni - Dar es salaam.

![](img/implementer_info.PNG)

 After providing all required  informations in the Implementer Information sections, System will prompty a declaration as shown below, then after accepting the declaration system will go on the next stage which is ** <span>Project Informations </span>** .<span style = "color:red">**Remember on the next stage implementer will submit only one project and after being validated and successfuly loggedin will able to register other 
 projects **</span>


![](img/declaration.PNG)


**<span style="color:Black;font-size:1.2em">2. Project Information</span>**

   i.** <span>Project Name. </span>** - This field receive project name executed or to be executed in the Local Government Authorities. <span style = "color:green">**(Type the name of the project in the form field) **</span>

   ii.** <span>Project Short Form </span>** -This field receive Initials which represent project name. <span style = "color:green">**(Type project shortname  in the form field) **</span>

   iii.** <span>Sector</span>** -Project Sector  -  <span style = "color:green">**(Select Sector from the drop down menu) **</span>  

   iv.** <span>Funder </span>** - Search Funder in the form field and then system will populate name of funder in the form field, after that select Funder in the form field. <span style = "color:green">**If the funder is USAID system will populate USAID priorities recomended by USAID on Tanzania projects as shown below and then select the priority from drop down list **</span> 
   ![](img/usaid.PNG)

   v.** <span>Total Fund in USD  </span>** - Total Project Fund .

   vi.** <span>Start Date </span>** -  <span style = "color:green">**(Select the start date of the project from the Calendar, that will pop up after clicking the start date field) **</span>  
    ![](img/start_date.PNG)

   vii.** <span>End Date </span>** - <span style = "color:green">**(Select the end date of the project from the Calendar, that will pop up after clicking the end date field) **</span>  
    ![](img/end_date.PNG)

  viii.** <span>Priority Area </span>** - <span style = "color:green">**(Priority Area Depends on the sector selected if the selector selected is Health Sector System will populate Health Sector Priority areas, etc) **</span>  
    ![](img/priority.PNG)
     ix.** <span>National Guideline </span>** - <span style = "color:green">**(This are the national focus that are required to be aligned with implementer projects) **</span>  
    ![](img/national_guide.PNG)

   Make sure all informations are filled correctly other wise you will get   .<span style = "color:red">**Wrong Information error on the correspondig stage **</span>
       ![](img/wrong_proj.PNG)
    After finishing the stage click next to go on another stage which is the <span> **Project Documents **</span>


**<span style="color:Black;font-size:1.2em">2. Project Documents</span>**

![](img/proj_documents.PNG)
   i.** <span>Introduction Letter.** </span>  <span style = "color:green">**(Upload Introduction Letter) **</span>
   
   ii.** <span>Memorandum of Understanding** </span> <span style = "color:green">**(Upload Memorandum of Understanding) **</span>

  After finishing the stage click next to go on another stage which is the <span> **Project Scope **</span>
      

      
**<span style="color:Black;font-size:1.2em">2. Project Scope</span>**

![](img/proj_Scope.PNG)
   ** <span>Select project location scope .</span>  <span style = "color:green">**(Region/Council if it implemented in the council scope select  Region then you will see its council ad if it implemented in the regions select regions in the drop down selections ) **</span>
![](img/proj_council.PNG)
      After completing this stage you will click finish and system will submit your informations 
      in case there is something wrong with the information submitted system will pop out the error message with the warning symbol.
      ![](img/proj_success.PNG)

  <span style = "color:red">**Note **</span>
   After Validations of the Implementer Informations provided, system will send email to the implementer with clear instructions to be followed in order to login in the System.